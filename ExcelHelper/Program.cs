﻿using NorthGate.Web.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System.ComponentModel;
using System.Reflection;
using static NorthGate.Web.Helpers.ExcelHelper;

namespace ExcelHelper
{

    internal class ExampleRun
    {
        private static void Main()
        {
            string pathSrcFolder = @"C:\temp";
            if(Directory.Exists(pathSrcFolder)==false)
                Directory.CreateDirectory(pathSrcFolder);
            
            string srcFilename = "ErrorTallyTemplate.xls";
            //srcFilename = "0215北門海運派件範本.xls";
            string sFilePath_ErrorTemplate = Path.Combine(pathSrcFolder, srcFilename);

            MemoryStream stream = new MemoryStream(File.ReadAllBytes(sFilePath_ErrorTemplate));
            WorkbookEntity<VendorFreightExcelModel> workbookEntity = new WorkbookEntity<VendorFreightExcelModel>(stream);
            workbookEntity.LoadAllSheets(srcFilename);
            //比對Model欄位格式正確性
            workbookEntity.ParseModel<VendorFreightExcelModel>();
            //workbookEntity.ParseModel<DeliveriedFreightExcelModel>();
            //開始著色，並拿到新的excel file MemoryStream，可以回傳給前端
            MemoryStream newStream = workbookEntity.GetProcessedStream_ByImportedTallyExcel();
            //MemoryStream newStream = workbookEntity.GetProcessedStream_ByImportedDeliveriedFreightExcel();
            
            //------------ 測試用  寫入本機excel檔案
            //string pathSrcFolder = @"C:\temp";
            string filenameToExcelFile = "coloredTally.xls";
            string pathToExcelFile = Path.Combine(pathSrcFolder, filenameToExcelFile);
            workbookEntity.SaveToLocal(pathToExcelFile);
        }
    }
}