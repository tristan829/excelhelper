﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NorthGate.Web.Helpers
{
    internal class ExcelHelper
    {
        public static string GetExceptionMessage(EnumExceptionType errType, string errMessage)
        {
            return $"Error Code: {(int)errType}{Environment.NewLine}Error Type: {errType}{Environment.NewLine}Error Message: {errMessage}";
        }
        public enum EnumExceptionType
        {
            ColumnCountNotMatch = 31
        }
        public class WorkbookEntity<T>
        {
            private MemoryStream _stream { get; set; }
            public IWorkbook Workbook;
            public List<SheetEntity<T>> ListAllSheets { get; set; } = new List<SheetEntity<T>>();
            public WorkbookEntity(MemoryStream stream)
            {
                this._stream = stream;
            }
            public enum EnumCellStyleType
            {
                /// <summary>
                /// 顏色 - Cell格式錯誤
                /// </summary>
                WrongCellFormat,
                /// <summary>
                /// 顏色 - 同一張提貨單，資料不一致
                /// </summary>
                DifferentDataInOnBill,
                /// <summary>
                /// 顏色 - 地址解析錯誤
                /// </summary>
                ParseAddressError,
                /// <summary>
                /// 顏色 - 預設白底黑字
                /// </summary>
                DefaultStyle,
            }

            public static List<CellStyleEntity> ListCellStyleEntity { get; set; } = new List<CellStyleEntity>();
            public void SetDefault_ListCellStyleEntity()
            {
                ListCellStyleEntity.Add(new CellStyleEntity(this.Workbook, EnumCellStyleType.WrongCellFormat, IndexedColors.Rose.Index, null));
                ListCellStyleEntity.Add(new CellStyleEntity(this.Workbook, EnumCellStyleType.DifferentDataInOnBill, IndexedColors.Aqua.Index, null));
                ListCellStyleEntity.Add(new CellStyleEntity(this.Workbook, EnumCellStyleType.ParseAddressError, IndexedColors.Red.Index, null));
                ListCellStyleEntity.Add(new CellStyleEntity(this.Workbook, EnumCellStyleType.DefaultStyle, IndexedColors.White.Index, IndexedColors.Black.Index));
            }
            public static CellStyleEntity GetCellStyleEntity(EnumCellStyleType enumCellStyle)
            {
                switch (enumCellStyle)
                {
                    case WorkbookEntity<T>.EnumCellStyleType.WrongCellFormat:
                    case WorkbookEntity<T>.EnumCellStyleType.DifferentDataInOnBill:
                    case WorkbookEntity<T>.EnumCellStyleType.ParseAddressError:
                    case WorkbookEntity<T>.EnumCellStyleType.DefaultStyle:
                        return WorkbookEntity<T>.ListCellStyleEntity.Where(x => x.CellStyleType == enumCellStyle).First();
                    default:
                        return WorkbookEntity<T>.ListCellStyleEntity.Where(x => x.CellStyleType == EnumCellStyleType.DefaultStyle).First();
                }
            }

            public class CellStyleEntity
            {
                public EnumCellStyleType CellStyleType { get; set; }
                public ICellStyle CellStyle { get; set; }
                public CellStyleEntity(IWorkbook workbook, EnumCellStyleType enumCellStyle, short bgColorCode, short? fontColorCode)
                {
                    //設定顏色
                    ICellStyle newCellStyle = workbook.CreateCellStyle();
                    //儲存格背景顏色
                    newCellStyle.FillForegroundColor = bgColorCode;
                    newCellStyle.FillPattern = FillPattern.SolidForeground;
                    if (fontColorCode != null)
                    {
                        IFont font = workbook.CreateFont();
                        font.Color = fontColorCode.GetValueOrDefault();
                        newCellStyle.SetFont(font);
                    }
                    // set props
                    this.CellStyleType = enumCellStyle;
                    this.CellStyle = newCellStyle;
                }
            }
            /// <summary>
            /// 分析匯入的理貨明細excel, 幫錯誤上顏色
            /// </summary>
            /// <returns></returns>
            public MemoryStream GetProcessedStream_ByImportedTallyExcel()
            {
                foreach (var eachSheet in this.ListAllSheets)
                {
                    eachSheet.FillColor_WrongFormat();
                    eachSheet.FillColor_DifferentDataInOneBill();
                    eachSheet.FillColor_AddressParseError();
                }
                MemoryStream memoryStream = new MemoryStream();
                this.Workbook.Write(memoryStream);
                return memoryStream;   //this._stream;
            }
            /// <summary>
            /// 分析匯入的批次更新送達貨件excel, 幫錯誤上顏色
            /// </summary>
            /// <returns></returns>
            public MemoryStream GetProcessedStream_ByImportedDeliveriedFreightExcel()
            {
                foreach (var eachSheet in this.ListAllSheets)
                {
                    eachSheet.FillColor_WrongFormat();
                    //eachSheet.FillColor_DifferentDataInOneBill(this.Workbook);
                    //eachSheet.FillColor_AddressParseError(this.Workbook);
                }
                MemoryStream memoryStream = new MemoryStream();
                this.Workbook.Write(memoryStream);
                return memoryStream;   //this._stream;
            }
            /// <summary>
            /// 分析匯入的批次匯入轉運單號excel, 幫錯誤上顏色
            /// </summary>
            /// <returns></returns>
            public MemoryStream GetProcessedStream_ByImportedTransferNumberExcel()
            {
                foreach (var eachSheet in this.ListAllSheets)
                {
                    eachSheet.FillColor_WrongFormat();
                    //eachSheet.FillColor_DifferentDataInOneBill(this.Workbook);
                    //eachSheet.FillColor_AddressParseError(this.Workbook);
                }
                MemoryStream memoryStream = new MemoryStream();
                this.Workbook.Write(memoryStream);
                return memoryStream;   //this._stream;
            }
            /// <summary>
            /// 分析匯入的批次匯入稅金excel, 幫錯誤上顏色
            /// </summary>
            /// <returns></returns>
            public MemoryStream GetProcessedStream_ByImportedTaxExcel()
            {
                foreach (var eachSheet in this.ListAllSheets)
                {
                    eachSheet.FillColor_WrongFormat();
                    //eachSheet.FillColor_DifferentDataInOneBill(this.Workbook);
                    //eachSheet.FillColor_AddressParseError(this.Workbook);
                }
                MemoryStream memoryStream = new MemoryStream();
                this.Workbook.Write(memoryStream);
                return memoryStream;   //this._stream;
            }
            /// <summary>
            /// 分析匯入的廠商帳款設定excel, 幫錯誤上顏色
            /// </summary>
            /// <returns></returns>
            public MemoryStream GetProcessedStream_ByImportedVendorPaymentInfoExcel()
            {
                foreach (var eachSheet in this.ListAllSheets)
                {
                    eachSheet.FillColor_WrongFormat();

                }
                MemoryStream memoryStream = new MemoryStream();
                this.Workbook.Write(memoryStream);
                return memoryStream;   //this._stream;
            }
            public void LoadAllSheets(string in_filename)
            {
                string fileExtension = Path.GetExtension(Path.GetFileName(in_filename));
                //string fileExtension = ".xls";

                switch (fileExtension)
                {
                    case ".xls":
                        this.Workbook = new HSSFWorkbook(this._stream);
                        break;
                    case ".xlsx":
                        this.Workbook = new XSSFWorkbook(this._stream);
                        break;
                    default:
                        this.Workbook = new HSSFWorkbook(this._stream);
                        break;
                }
                this.SetDefault_ListCellStyleEntity();
                int iSheetCount = this.Workbook.NumberOfSheets;
                for (int i = 0; i < iSheetCount; i++)
                {
                    SheetEntity<T> sheetEntity = new SheetEntity<T>();
                    sheetEntity.RootWorkbook = this.Workbook;
                    ISheet sheet = this.Workbook.GetSheetAt(i);
                    sheetEntity.LoadSheet(sheet, i);
                    this.ListAllSheets.Add(sheetEntity);
                }
            }
            public void ParseModel<T>() where T : new()
            {
                foreach (var eachSheetEntity in this.ListAllSheets)
                    eachSheetEntity.Load_ModelMatch_ExceptionInfos<T>(eachSheetEntity.SheetBody);
            }
            public void SaveToLocal(string in_toLocalFilePath)
            {
                FileStream fs = null;
                try
                {
                    fs = new FileStream(in_toLocalFilePath, FileMode.Create);
                    this.Workbook.Write(fs);
                    fs.Close();

                }
                catch (Exception ex)
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                    throw ex;
                }
                finally
                {
                    fs.Close();
                }
            }
        }
        /// <summary>
        /// 自訂Cell物件
        /// </summary>
        public class CellEntity
        {
            /// <summary>
            /// row位置
            /// </summary>
            public int Row { get; set; }
            /// <summary>
            /// col位置
            /// </summary>
            public int Col { get; set; }
            /// <summary>
            /// cell值
            /// </summary>
            public string? CellValue { get; set; }
        }
        /// <summary>
        /// 自訂Column物件
        /// </summary>
        public class ColumnEntity
        {
            /// <summary>
            /// col位置
            /// </summary>
            public int Col { get; set; }
            /// <summary>
            /// col名稱
            /// </summary>
            public string? Name { get; set; }
            /// <summary>
            /// 此Column底下所包含的Cells
            /// </summary>
            public List<CellEntity> ListCellEntity { get; set; } = new List<CellEntity>();
            /// <summary>
            /// 載入Cells
            /// </summary>
            /// <param name="in_sheet">單個Sheet</param>
            /// <param name="in_col">column位置</param>
            /// <param name="in_colName">column名稱</param>
            public void LoadListCellEntity(ISheet in_sheet, int in_col, string in_colName)
            {
                this.Col = in_col;
                this.Name = in_colName;
                this.ListCellEntity = new List<CellEntity>();
                //row總數
                int rowCount = ExcelHelper.PhysicalRowsCount(in_sheet);

                //讀取單一個Column下的cell
                for (int iRow = 0; iRow < rowCount; iRow++)
                {
                    //固定column , 變動row
                    var row = (IRow)in_sheet.GetRow(iRow);
                    ICell cell = row.GetCell(in_col);
                    if (cell == null)
                        continue;
                    CellEntity cellEntity = new CellEntity();
                    cellEntity.Row = iRow;
                    cellEntity.Col = in_col;
                    switch (cell.CellType)
                    {
                        case CellType.Numeric:
                            cellEntity.CellValue = cell == null ? "" : cell.NumericCellValue.ToString();
                            break;
                        case CellType.Unknown:
                        case CellType.Formula:
                        case CellType.Blank:
                        case CellType.String:
                            cellEntity.CellValue = cell == null ? "" : cell.ToString();
                            break;
                        case CellType.Boolean:
                            cellEntity.CellValue = cell == null ? "" : cell.BooleanCellValue.ToString();
                            break;
                        case CellType.Error:
                            cellEntity.CellValue = cell == null ? "" : cell.ErrorCellValue.ToString();
                            break;
                        default:
                            cellEntity.CellValue = cell == null ? "" : cell.ToString();
                            break;
                    }

                    if (cellEntity.CellValue != null)
                        cellEntity.CellValue = cellEntity.CellValue.Trim();
                    //放到Cell全索引
                    this.ListCellEntity.Add(cellEntity);
                }
            }
        }
        public class CellExceptionInfo
        {
            public short ColIndex { get; set; }
            public int RowIndex { get; set; }
            public string? PropName { get; set; }
            public string? PropType { get; set; }
            public string? PropValue { get; set; }
            public string? Message { get; set; }
            public short ColorCode { get; set; }
        }
        /// <summary>
        /// 自訂Row物件
        /// </summary>
        public class RowEntity<T>
        {
            /// <summary>
            /// row位置
            /// </summary>
            public int RowIndex { get; set; }
            /// <summary>
            /// 顏色 - Cell格式錯誤
            /// </summary>
            public static short colorWrongCellFormat = IndexedColors.Rose.Index;
            /// <summary>
            /// 顏色 - 同一張提貨單，資料不一致
            /// </summary>
            public static short colorDifferentDataInOnBill = IndexedColors.Aqua.Index;
            /// <summary>
            /// 顏色 - 地址解析錯誤
            /// </summary>
            public static short colorParseAddressError = IndexedColors.Red.Index;
            /// <summary>
            /// 此Row所包含的Cells
            /// </summary>
            public List<CellEntity> ListCellEntity { get; set; } = new List<CellEntity>();
            /// <summary>
            /// Cell解析錯誤清單
            /// </summary>
            public List<CellExceptionInfo> ListCellModelExceptionInfos { get; set; } = new List<CellExceptionInfo>();
            //public VendorFreightExcelModel vendorFreightExcelModel { get; set; } = new VendorFreightExcelModel();
            public object ObjModel { get; set; }
            public T GetDataModelByCellEntity<T>() where T : new()
            {
                T modelData = new T();
                if (this.ListCellEntity.Count == 0)
                    return modelData;
                Type t = modelData.GetType();
                PropertyInfo[] props = t.GetProperties();
                if (props.Count() != this.ListCellEntity.Count)
                    throw new Exception(GetExceptionMessage(EnumExceptionType.ColumnCountNotMatch, "DataModel的屬性個數與Excel的欄位個數不符!"));
                for (short i = 0; i < props.Length; i++)
                {
                    var eachProp = props[i];
                    string propType = eachProp.PropertyType.Name;
                    var cellValue = this.ListCellEntity[i].CellValue.Trim();
                    try
                    {
                        switch (propType)
                        {
                            case "DateTime":
                                DateTime dt = DateTime.ParseExact(cellValue, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                eachProp.SetValue(modelData, dt);
                                break;
                            case "Int16":
                                eachProp.SetValue(modelData, Int16.Parse(cellValue));
                                break;
                            case "Decimal":
                                if (cellValue == "")
                                    eachProp.SetValue(modelData, Decimal.Parse("0"));
                                else
                                    eachProp.SetValue(modelData, Decimal.Parse(cellValue));
                                break;
                            default:
                                eachProp.SetValue(modelData, cellValue);
                                break;
                        }

                    }
                    catch (Exception e)
                    {
                        CellExceptionInfo cellExceptionInfo = new CellExceptionInfo();
                        cellExceptionInfo.ColIndex = i;
                        cellExceptionInfo.RowIndex = this.RowIndex;
                        cellExceptionInfo.PropName = eachProp.Name;
                        cellExceptionInfo.PropValue = this.ListCellEntity[i].CellValue;
                        cellExceptionInfo.PropType = eachProp.PropertyType.Name;
                        cellExceptionInfo.Message = e.Message;
                        //設定錯誤顏色
                        cellExceptionInfo.ColorCode = RowEntity<T>.colorWrongCellFormat;
                        this.ListCellModelExceptionInfos.Add(cellExceptionInfo);
                    }
                }

                return modelData;

            }

            /// <summary>
            /// 載入Cells
            /// </summary>
            /// <param name="in_sheet">單個Sheet</param>
            /// <param name="in_rowIndex">row位置</param>
            /// <param name="in_colCounts">col總數目</param>
            public void LoadListCellEntity(ISheet in_sheet, int in_rowIndex, int in_colCounts)
            {
                IRow thisRow = in_sheet.GetRow(in_rowIndex);
                if (thisRow == null)
                    return;
                this.RowIndex = in_rowIndex;


                //讀取單一個Row下的cell
                for (int iCol = 0; iCol < in_colCounts; iCol++)
                {
                    //固定row , 變動col
                    ICell cell = thisRow.GetCell(iCol);
                    CellEntity cellEntity = new CellEntity();
                    cellEntity.Row = in_rowIndex;
                    cellEntity.Col = iCol;
                    cellEntity.CellValue = cell == null ? "" : cell.ToString();
                    if (cellEntity.CellValue != null)
                        cellEntity.CellValue = cellEntity.CellValue.Trim();
                    this.ListCellEntity.Add(cellEntity);
                }
            }
        }
        /// <summary>
        /// 自訂Sheet物件
        /// </summary>
        public class SheetEntity<T>
        {
            /// <summary>
            /// Sheet位置
            /// </summary>
            public int SheetNumber { get; set; }
            /// <summary>
            /// Sheet名稱
            /// </summary>
            public string? SheetName { get; set; }
            /// <summary>
            /// ISheet本體
            /// </summary>
            public ISheet? SheetBody { get; set; }
            /// <summary>
            /// 所屬的Workbook
            /// </summary>
            public IWorkbook RootWorkbook { get; set; }
            /// <summary>
            /// Column數目
            /// </summary>
            public int ColumnCount { get; set; }
            public List<ColumnEntity> listAllColumns { get; set; } = new List<ColumnEntity>();
            /// <summary>
            /// Row全索引
            /// </summary>
            public List<RowEntity<T>> listAllDataRows { get; set; } = new List<RowEntity<T>>();
            /// <summary>
            /// Cell解析錯誤清單
            /// </summary>
            public List<CellExceptionInfo> ListCellModelExceptionInfos { get; set; } = new List<CellExceptionInfo>();
            /// <summary>
            /// //Cell全索引
            /// </summary>
            public List<CellEntity> listAllCells { get; set; } = new List<CellEntity>();
            /// <summary>
            /// 讀取資料
            /// </summary>
            /// <param name="in_sheet">ISheet本體</param>
            /// <param name="in_iSheetNumber">Sheet位置</param>
            public void LoadSheet(ISheet in_sheet, int in_iSheetNumber)
            {
                this.SheetNumber = in_iSheetNumber;
                this.SheetBody = in_sheet;
                this.SheetName = in_sheet.SheetName;

                if (this.SheetBody == null)
                    return;
                // Load ListAllColumns
                IRow firstRow = this.SheetBody.GetRow(0);
                if (firstRow == null)
                    return;
                this.ColumnCount = firstRow.Cells.Count;
                for (int i = 0; i < this.ColumnCount; i++)
                {
                    // each column
                    ColumnEntity column = new ColumnEntity();
                    ICell cell = firstRow.Cells[i];
                    if (cell == null)
                        continue;
                    string colName = cell.ToString() == null ? "" : cell.ToString().Trim();
                    if (colName == "")
                        continue;
                    column.LoadListCellEntity(this.SheetBody, i, colName);
                    // 放到column全索引
                    this.listAllColumns.Add(column);
                    // 放到cell全索引
                    this.listAllCells.AddRange(column.ListCellEntity);
                }
                //set sheet default color => background white, font color black
                this.SetDefaultStyleColor();
                // Load ListAllRows
                int iRowCount = ExcelHelper.PhysicalRowsCount(in_sheet);

                for (int iRow = 0; iRow < iRowCount; iRow++)
                {
                    IRow row = this.SheetBody.GetRow(iRow);
                    RowEntity<T> rowEntity = new RowEntity<T>();
                    rowEntity.LoadListCellEntity(this.SheetBody, iRow, this.ColumnCount);
                    listAllDataRows.Add(rowEntity);
                }


            }
            public void SetDefaultStyleColor()
            {
                int iRowCount = ExcelHelper.PhysicalRowsCount(this.SheetBody);

                for (int iRow = 1; iRow < iRowCount; iRow++)
                {
                    IRow row = this.SheetBody.GetRow(iRow);
                    for (int iCol = 0; iCol < this.ColumnCount - 1; iCol++)
                    {
                        var cell = row.GetCell(iCol);
                        if (cell == null)
                            continue;
                        if (cell.CellStyle == null)
                            continue;
                        this.FillCellColor(iRow, iCol, WorkbookEntity<T>.EnumCellStyleType.DefaultStyle);
                    }
                }
            }
            public void Load_ModelMatch_ExceptionInfos<T>(ISheet in_sheet) where T : new()
            {
                //List<CellExceptionInfo> list_RTN_CellExceptionInfo = new List<CellExceptionInfo>();
                // Load ListAllRows
                int iRowCount = ExcelHelper.PhysicalRowsCount(in_sheet);  //刪除資料後,一樣會顯示原始行號 
                int rows = 0;
                for (int h = 1; h < iRowCount; h++)
                {
                    IRow row = (IRow)in_sheet.GetRow(h);
                    if (row.Cells.All(d => d.CellType == CellType.Blank))
                    {
                        rows++;
                        continue;
                    }
                }
                iRowCount = iRowCount - rows;

                //不含Row[0] 欄位名稱|欄位名稱|欄位名稱...., 轉換excel string資料 to data model
                for (int iRow = 1; iRow < iRowCount; iRow++)
                {
                    object objRowEntity = listAllDataRows[iRow];
                    RowEntity<T> rowEntity = (RowEntity<T>)objRowEntity;
                    //轉換資料並產生 rowEntity.ListCellExceptionInfos 錯誤清單
                    var newDataModel = rowEntity.GetDataModelByCellEntity<T>();
                    rowEntity.ObjModel = newDataModel;
                    listAllDataRows[iRow].ObjModel = newDataModel;
                    //將rowEntity.ListCellExceptionInfos 加入 sheet.ListCellExceptionInfos 總錯誤清單
                    this.ListCellModelExceptionInfos.AddRange(rowEntity.ListCellModelExceptionInfos);
                }
            }

            public void defaultStytleColo(IWorkbook in_workbook)
            {
                int iRowCount = ExcelHelper.PhysicalRowsCount(this.SheetBody);

                for (int iRow = 1; iRow < iRowCount; iRow++)
                {
                    IRow row = this.SheetBody.GetRow(iRow);
                    for (int j = 0; j < this.ColumnCount - 1; j++)
                    {
                        ICell cell = row.GetCell(j);
                        //設定顏色
                        ICellStyle CellStyle = in_workbook.CreateCellStyle();
                        //儲存格背景顏色
                        CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.COLOR_NORMAL;  // 64;  //setfillforegroundcolor
                        CellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;//Thick;//粗
                        CellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;//細實線
                        CellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;//Hair;//虛線
                        CellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;//Dotted;//...  
                                                                                 //     CellStyle.FillPattern = FillPattern.SolidForeground;  //solidforeground
                        cell.CellStyle = CellStyle;
                    }
                }
            }


            private void FillCellColor(int rowIndex, int colIndex, WorkbookEntity<T>.EnumCellStyleType enumCellStyleType)
            {
                IRow row = this.SheetBody.GetRow(rowIndex);
                ICell cell = row.GetCell(colIndex) ?? row.CreateCell(colIndex);
                var cellStyleEntity = WorkbookEntity<T>.GetCellStyleEntity(enumCellStyleType);
                //合併儲存格設定mergeCellStyle的格式
                cell.CellStyle = cellStyleEntity.CellStyle;
            }
            public void FillColor_WrongFormat()
            {
                foreach (var exceptionCellInfo in this.ListCellModelExceptionInfos)
                {
                    this.FillCellColor(exceptionCellInfo.RowIndex, exceptionCellInfo.ColIndex, WorkbookEntity<T>.EnumCellStyleType.WrongCellFormat);
                }
            }
            /// <summary>
            /// 找出相同提貨單號，但是資料不一致的提貨單號碼
            /// </summary>
            /// <returns></returns>

            public List<RowEntity<VendorFreightExcelModel>> GetList_RowIndex_DifferentDataInOneBill()
            {
                List<VendorFreightExcelModel> listVendorFreightExcelModels = new List<VendorFreightExcelModel>();
                //合併ROW data model to listVendorFreightExcelModels
                //Row[0]是欄位名稱，不要加入
                for (int iRow = 1; iRow < this.listAllDataRows.Count; iRow++)
                {
                    VendorFreightExcelModel vendorFreightExcelModel = (VendorFreightExcelModel)this.listAllDataRows[iRow].ObjModel;
                    listVendorFreightExcelModels.Add(vendorFreightExcelModel);
                }

                //相同 提貨單號/客戶地址/客戶名稱/客戶電話/派送公司
                var qGroupedVendorFreight = listVendorFreightExcelModels.GroupBy(x => new { x.LadingBillNo, x.CustomerAddress, x.CustomerName, x.CustomerPhoneNo, x.DeliveryCompany },
                (key, group) => new
                {
                    LadingBillNo = key.LadingBillNo,
                    CustomerAddress = key.CustomerAddress,
                    CustomerName = key.CustomerName,
                    CustomerPhoneNo = key.CustomerPhoneNo,
                    DeliveryCompany = key.DeliveryCompany,
                    groupedData = group.ToList()
                });
                //找出相同提貨單號，但是資料不一致的提貨單
                var qDuplicatedLadingBillNo = qGroupedVendorFreight.GroupBy(x => new { x.LadingBillNo },
                (key, group) => new
                {
                    LadingBillNo = key.LadingBillNo,
                    //CustomerAddress = key.CustomerAddress,
                    //CustomerName = key.CustomerName,
                    //CustomerPhoneNo = key.CustomerPhoneNo,
                    //DeliveryCompany = key.DeliveryCompany,
                    Count = group.ToList().Count()
                });
                //foreach (var each in qGroupedVendorFreight)
                //{
                //    Console.WriteLine($"    LadingBillNo: {each.LadingBillNo}");
                //    Console.WriteLine($"    CustomerAddress: {each.CustomerAddress}");
                //    Console.WriteLine($"    CustomerName: {each.CustomerName}");
                //    Console.WriteLine($"    CustomerPhoneNo: {each.CustomerPhoneNo}");
                //    Console.WriteLine($"    DeliveryCompany: {each.DeliveryCompany}");

                //    Console.WriteLine($"        Count: {each.Result.Count}");
                //    Console.WriteLine("----");
                //}
                var listDuplicatedLadingBillNo = qDuplicatedLadingBillNo.Where(x => x.Count > 1).ToList();
                //foreach (var each in qDuplicatedLadingBillNo)
                //{
                //    Console.WriteLine($"    LadingBillNo: {each.LadingBillNo}");
                //    Console.WriteLine($"        Count: {each.Count}");
                //    Console.WriteLine("----");
                //}
                List<string> slist_DataDifferent_LadingBillNo = listDuplicatedLadingBillNo.Select(x => x.LadingBillNo).ToList();
                List<RowEntity<VendorFreightExcelModel>> listDifferentDataInOneBill_Model = new List<RowEntity<VendorFreightExcelModel>>();

                for (int i = 1; i < this.listAllDataRows.Count; i++)
                {
                    object obj = this.listAllDataRows[i];
                    RowEntity<VendorFreightExcelModel> eachRowEntity = (RowEntity<VendorFreightExcelModel>)obj;
                    var rowModel = (VendorFreightExcelModel)eachRowEntity.ObjModel;
                    //RowEntity<VendorFreightExcelModel> rowEntity = (VendorFreightExcelModel)eachRowEntity.ObjModel
                    if (slist_DataDifferent_LadingBillNo.Contains(rowModel.LadingBillNo))
                        listDifferentDataInOneBill_Model.Add(eachRowEntity);
                }

                //listDifferentDataInOneBill_Model = (from p in this.listAllDataRows
                //                                    where slist_DataDifferent_LadingBillNo.Contains(p.ObjModel.LadingBillNo)
                //                                    select p).ToList();
                //Row[0]是欄位名稱，不要加入
                //List<int> ilistRTN = (from p in this.listAllDataRows
                //                      where slist_DataDifferent_LadingBillNo.Contains(p.vendorFreightExcelModel.LadingBillNo)
                //                      select p.RowIndex).ToList();
                return listDifferentDataInOneBill_Model;
            }
            public void FillColor_DifferentDataInOneBill()
            {
                int iCount = 0;
                //取得異常單號 DifferentDataInOneBill
                List<RowEntity<VendorFreightExcelModel>> listDifferentDataInOneBil = this.GetList_RowIndex_DifferentDataInOneBill();
                List<int> ilsitRowInex_RowIndex_DifferentDataInOneBil = listDifferentDataInOneBil.Select(x => x.RowIndex).ToList();
                foreach (var rowIndex in ilsitRowInex_RowIndex_DifferentDataInOneBil)
                {
                    IRow row = this.SheetBody.GetRow(rowIndex);
                    ICell cell = row.GetCell(3);
                    if (cell == null)
                        continue;
                    const int iTallyBillNo_col = 3;
                    this.FillCellColor(rowIndex, iTallyBillNo_col, WorkbookEntity<T>.EnumCellStyleType.DifferentDataInOnBill);
                }
            }

            public void FillColor_AddressParseError()
            {

                //get address column header
                RowEntity<T> firstRow = this.listAllDataRows.FirstOrDefault();
                if (firstRow == null)
                    return;
                var cellHeaderAddress = firstRow.ListCellEntity.Where(x => x.CellValue.Trim() == "客戶住址").FirstOrDefault();
                if (cellHeaderAddress == null)
                    throw new Exception("在Excel第一個Row找不到標題為[客戶住址]!");
                ColumnEntity columnAddress = this.listAllColumns.Where(x => x.Name == "客戶住址").FirstOrDefault();
                if (columnAddress == null)
                    throw new Exception("在Excel中找不到ColumnEntity-Name為[客戶住址]!");

                List<CellEntity> listCell_ErrorParseAddress = new List<CellEntity>();
                // i[0] 是Header Name, 故從1開始, 分析每一個住址字串
                for (int i = 1; i < columnAddress.ListCellEntity.Count; i++)
                {
                    CellEntity cell = columnAddress.ListCellEntity[i];
                    AddressHelper addressParser = new AddressHelper(cell.CellValue);
                    if (addressParser.IsParseSuccessed == false)
                        listCell_ErrorParseAddress.Add(cell);
                }

                foreach (var errCell in listCell_ErrorParseAddress)
                {
                    IRow row = this.SheetBody.GetRow(errCell.Row);
                    ICell cell = row.GetCell(errCell.Col);
                    if (cell == null)
                        continue;
                    this.FillCellColor(errCell.Row, errCell.Col, WorkbookEntity<T>.EnumCellStyleType.ParseAddressError);
                }
            }

        }
        /// <summary>
        /// 匯出Excel, 並依照Exceptions著色
        /// </summary>
        /// <param name="in_listRowEntity"></param>
        /// <returns></returns>
        //public string GetExceptionColorExcel(List<RowEntity> in_listRowEntity)
        //{

        //}
        /// <summary>
        /// 真正的LastRowIndex
        /// </summary>
        /// <param name="aExcelSheet"></param>
        /// <returns></returns>
        public static int LastRowIndex(ISheet aExcelSheet)
        {
            IEnumerator rowIter = aExcelSheet.GetRowEnumerator();
            return rowIter.MoveNext()
            ? aExcelSheet.LastRowNum
            : -1;
        }

        /// <summary>
        /// 真正的 row count, 如果用 sheet.LastRowNum | row.Cells.Count 都會少1
        /// </summary>
        /// <param name="aExcelSheet"></param>
        /// <returns></returns>
        //public static int RowsSpanCount(ISheet aExcelSheet)
        //{
        //    return aExcelSheet.LastRowNum + 1;
        //}
        /// <summary>
        /// 真正的 row count, 如果用 sheet.LastRowNum | row.Cells.Count 都會少1
        /// </summary>
        /// <param name="aExcelSheet"></param>
        /// <returns></returns>
        public static int PhysicalRowsCount(ISheet aExcelSheet)
        {
            if (aExcelSheet == null)
            {
                return 0;
            }

            int rowsCount = 0;
            IEnumerator rowEnumerator = aExcelSheet.GetRowEnumerator();
            while (rowEnumerator.MoveNext())
            {
                ++rowsCount;
            }

            return rowsCount;
        }

        /// <summary>
        /// 按照Excel欄位順序規劃的 data model
        /// </summary>
        public partial class VendorFreightExcelModel
        {
            /// <summary>
            /// 日期
            /// </summary>
            [Required]
            public DateTime LadingBillDate { get; set; }
            /// <summary>
            /// 廠商名稱
            /// </summary>
            [Required]
            public string? ExcelVendorName { get; set; }
            /// <summary>
            /// 清關條碼
            /// </summary>
            [Required]
            public string CustomsClearanceCode { get; set; } = null!;
            /// <summary>
            /// 提單號碼
            /// </summary>
            [Required]
            public string LadingBillNo { get; set; } = null!;
            /// <summary>
            /// 序號
            /// </summary>
            [Required]
            public string SerialNumber { get; set; } = null!;
            /// <summary>
            /// 件數
            /// </summary>
            [Required]
            public short FreightCount { get; set; }
            /// <summary>
            /// 重量
            /// </summary>
            [Required]
            public decimal Weight { get; set; }
            /// <summary>
            /// 客戶名稱
            /// </summary>
            [Required]
            public string CustomerName { get; set; } = null!;
            /// <summary>
            /// 客戶住址
            /// </summary>
            [Required]
            public string CustomerAddress { get; set; } = null!;
            /// <summary>
            /// 客戶電話
            /// </summary>
            [Required]
            public string CustomerPhoneNo { get; set; } = null!;
            /// <summary>
            /// 代收款
            /// </summary>
            public decimal CollectionMoney { get; set; }
            /// <summary>
            /// 派件公司 (轉站廠商名稱)
            /// </summary>
            [Required]
            public string DeliveryCompany { get; set; } = null!;
            /// <summary>
            /// 備註
            /// </summary>
            [Required]
            public string Remark { get; set; } = null!;
            /// <summary>
            /// 轉站
            /// </summary>
            public string ExcelTransferName { get; set; } = null!;
        }
    }
}


