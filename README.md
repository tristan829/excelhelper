# Get Start

1. 在c:\temp創建資料夾，並把錯誤範例excel檔案放進去，這個資料夾未來也會產生colorTally.xls
2. 自行定義一個Model，提供給ExcelHelper比對欄位格式正確性，例如:
```cs
public class DeliveriedFreightExcelModel
{
    /// <summary>
    /// 貨件序號
    /// </summary>
    [Required]
    public string SerialNumber { get; set; } = null!;
    /// <summary>
    /// 送達日期
    /// </summary>
    [Required]
    public DateTime DeliveriedDate { get; set; }
}
```
3. 主要使用如下，先準備好一個MemorySteam，可以load from file or receive from web api
```cs
MemoryStream stream = new MemoryStream(File.ReadAllBytes(sFilePath_ErrorTemplate));
WorkbookEntity<DeliveriedFreightExcelModel> workbookEntity = new WorkbookEntity<DeliveriedFreightExcelModel>(stream);
workbookEntity.LoadAllSheets(srcFilename);
//比對Model欄位格式正確性
workbookEntity.ParseModel<DeliveriedFreightExcelModel>();
//MemoryStream newStream = workbookEntity.GetProcessedStream_ByImportedTallyExcel();
//開始著色，並拿到新的excel file MemoryStream，可以回傳給前端
MemoryStream newStream = workbookEntity.GetProcessedStream_ByImportedDeliveriedFreightExcel();
```